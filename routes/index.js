const express = require("express");
const router = express.Router();


const userRouter = require("./user");
const theatreRouter = require('./theatre');
const movieRouter = require('./movie');
const seatRouter = require('./seat');
const userSeatRouter = require('./userSeat')

module.exports = (app) => {
    app.use("/user", userRouter)
    app.use('/theatre', theatreRouter)
    app.use('/movie', movieRouter)
    app.use('/seat', seatRouter)
    app.use('/booked', userSeatRouter)
};