const express = require("express");
const router = express.Router();
const passport = require("passport")


const { listAvailableSeats, bookSeat, resetSeats } = require('../controllers/seatController')

router.get('/:theatreId/:movieId',  passport.authenticate("tokenVerification", { session: false }) ,listAvailableSeats)
router.put('/:theatreId/:movieId', bookSeat)
router.put('/reset/:theatreId/:movieId', resetSeats)


module.exports = router