const express = require("express");
const router = express.Router();
const passport = require("passport")

const upload = require('../middleware/upload')

const { addMovie, listMovies, movieTheatre } = require('../controllers/movieController')

router.post('/add', upload.single('movieImage'), addMovie)
router.post('/add/theatre', movieTheatre)


router.get('/', passport.authenticate("tokenVerification", { session: false }),  listMovies)

module.exports = router