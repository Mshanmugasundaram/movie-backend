const express = require("express");
const router = express.Router();
const passport = require("passport")


const { addTheatre, listTheatre, movieTheatres, ownerTheatre } = require('../controllers/theatreController')

router.post('/add', addTheatre);
router.get('/', passport.authenticate("tokenVerification", { session: false }),listTheatre)
router.get('/:movieId', passport.authenticate("tokenVerification", { session: false }), movieTheatres)
router.get('/owner/:ownerId', passport.authenticate("tokenVerification", { session: false }), ownerTheatre)

module.exports = router
