const express = require("express");
const router = express.Router();
const passport = require("passport")

const { listUserSeat } = require('../controllers/userSeatController')

router.get('/', passport.authenticate("tokenVerification", { session: false }) ,listUserSeat);

module.exports = router