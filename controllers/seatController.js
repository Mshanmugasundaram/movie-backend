const Seat = require("../models/seatSchema");
const Movie = require("../models/movieSchema")
const UserSeat = require('../models/userSeatSchema')
const Theatre = require('../models/theatreSchema')
const MovieTheatre = require('../models/movieTheatreSchema')

const listAvailableSeats = async(req, res, next) => {
    let {movieId, theatreId} = req.params
    try {
            let movieSeats = await MovieTheatre.findOne({"movieId": movieId, 'theatreId': theatreId})
            console.log(movieSeats.reserved.length)
            return next({apiStatus: "SUCCESS", data: movieSeats, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const bookSeat = async(req, res, next) => {
    let { movieId, theatreId } = req.params
    let { seats, userId } = req.body
     console.log(seats)
    try {
        let movie = await MovieTheatre.findOne({movieId: movieId, theatreId: theatreId})
        let reservedSeats = movie.reserved
        if(movie == null) {
            return next({apiStatus: "FAILURE", errorMessage: "NOT_FOUND", statusCode: 404})
        }
        for(let i=0; i<seats.length; i++) {
            reservedSeats.push(seats[i]);
            let userSeat = new UserSeat({
                userId,
                movieId,
                theatreId,
                seatNumber: seats[i]
            })
            await userSeat.save()
        }
        await movie.updateOne({
            reserved: reservedSeats
        })


        return next({apiStatus:"SUCCESS", data: {"message":"BOOKING_HAS_BEEN_SUCCESSFULLY_COMPLETED"}, statusCode:200})

    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const resetSeats = async(req, res, next) => {
    let {theatreId, movieId} = req.params 
    try {
        let reset = await MovieTheatre.updateOne({theatreId: theatreId, movieId: movieId}, {$set: { reserved: [] }})
        // console.log(reset)
        return next({apiStatus:"SUCCESS", data:"SEATS_RESTARTED_SUCCESSFULLY", statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}
module.exports = {
    listAvailableSeats,
    bookSeat,
    resetSeats
}