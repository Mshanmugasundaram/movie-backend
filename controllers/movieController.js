const Movie = require('../models/movieSchema');
const Theatre = require("../models/theatreSchema");
const Seat = require('../models/seatSchema');
const MovieTheatre = require('../models/movieTheatreSchema')


const addMovie = async(req, res, next) => {
    let {name, description} = req.body
    try {
        let movie = await Movie.findOne({'name': name})

        if(movie == null) {
            let newMovie = new Movie({
                name,
                description
            })
            if(req.file) {
                newMovie.movieImage= req.file.path
            }
            await newMovie.save();
        } else {
            return next({apiStatus:"FAILURE", data: {"message":"MOVIE_ALREADY_ADDED"}, statusCode: 409})
        }

        return next({apiStatus:"SUCCESS", data: {"message":"MOVIE_ADDED_SUCCESSFULLY"}, statusCode: 200})
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const movieTheatre = async(req, res, next) => {
    let { name, theatreName, seat, reserved } = req.body
    try {
        let movie = await Movie.findOne({name: name})
        if(movie == null) {
            return next({apiStatus: "FAILURE", errorMessage: "MOVIE_DOESN'T FOUND", statusCode: 404})
        }
        let theatre = await Theatre.findOne({name: theatreName})
        let movieTheatre = await MovieTheatre.findOne({movieId: movie._id, theatreId: theatre._id})
        if(movieTheatre != null) {
            await movieTheatre.updateOne({seat: seat})
            return next({apiStatus: "SUCCESS", data: "ALREADY_EXSISTS", statusCode: 204})
        } else {
            let theatreMovie = new MovieTheatre({
                movieId: movie._id,
                theatreId: theatre._id,
                seat: seat,
                reserved: reserved 
            })
            await theatreMovie.save()
            return next({apiStatus: "SUCCESS", data: "MOVIE_ADDED_SUCCESSFULLY", statusCode: 200})
        }
    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const listMovies = async(req, res, next) => {
    let movies = await Movie.find({}).sort({"createdAt": -1})
    return next({apiStatus: "SUCCESS", data: movies, statusCode: 200})
}


module.exports = {
    addMovie,
    listMovies,
    movieTheatre
}