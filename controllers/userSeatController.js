const UserSeat = require("../models/userSeatSchema")
const Theatre = require("../models/theatreSchema")
const Movie = require("../models/movieSchema")

const listUserSeat = async(req, res, next) => {
    let {userId} = req.body
    let bookedSeats = await UserSeat.find({userId: req.user._id})
    console.log(bookedSeats)
    let seats=[]
    let movieNames = []
    let theatreNames = []
    for(let i=0; i<bookedSeats.length; i++) {
        seats.push(bookedSeats[i].seatNumber)
        let movieDetails = await Movie.findOne({_id: bookedSeats[i].movieId})
        movieNames.push(movieDetails.name)
        let theatreDetails = await Theatre.findOne({_id: bookedSeats[i].theatreId})
        theatreNames.push(theatreDetails.name)
    }
    return next({apiStatus: "SUCCESS",
    data: {
        seats: seats,
        movie: movieNames,
        theatre: theatreNames
    }, statusCode: 200})
}

module.exports = {
    listUserSeat
}