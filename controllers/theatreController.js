const Theatre = require("../models/theatreSchema")
const MovieTheatre = require('../models/movieTheatreSchema')


const addTheatre = async (req, res, next) => {
    let {name, location, ownerId} = req.body
    let theatre = await Theatre.exists({name: name, location: location})
    if(theatre) {
        return next({apiStatus:"FAILURE", errorMessage:"THEATRE_ALREADY_EXISTS", statusCode: 409})
    }
    let newTheatre = new Theatre({
        name,
        location,
        ownerId
    })
    await newTheatre.save();
    return next({apiStatus:"SUCCESS", data: newTheatre, statusCode:200})
}

const listTheatre = async(req, res, next) => {
    let {name, location, perPage, pageNumber} = req.query
    perPage = perPage != undefined ? parseInt(perPage) : 10;
    pageNumber = pageNumber != undefined ? parseInt(pageNumber) : 1;
    const theatreQuery = Theatre.find({})
    theatreQuery.setOptions({lean: true});
    theatreQuery.collection(Theatre.collection);
    if(name != undefined) {
        await theatreQuery.find({"name": {$regex: ".*"+name+".*", $options:"i"}})
    }

    if( location != undefined) {
        await theatreQuery.find({"location": {$regex: ".*"+location+".*", $options:"i"}})
    }

    let theatres = await theatreQuery.sort({"createdAt": -1}).skip((pageNumber - 1)* perPage).limit(perPage).exec();
    return next({apiStatus: "SUCCESS", data: theatres, statusCode: 200})
}

const movieTheatres = async(req, res, next) => {
    let {movieId} = req.params
    try {
        let theatreDetails = []
        let theatres = await MovieTheatre.find({movieId: movieId})
        if (theatres == null) {
            return next({
                apiStatus:"Failure",
                errorMessage: "THEATRES_NOT_FOUND",
                statusCode: 404
            })
        } else {
            for(let i=0; i<theatres.length; i++) {
                let theatre = await Theatre.findById({"_id": theatres[i].theatreId})
                theatreDetails.push(theatre)
            }
        }
        return next({
            apiStatus:"SUCCESS",
            data: theatreDetails,
            statusCode: 200
        })

    } catch(err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}


const ownerTheatre = async(req, res, next) => {
    let {ownerId} = req.params
    let ownerTheatres = await Theatre.find({ownerId: ownerId})
    return next({
        apiStatus:"SUCCESS",
        data: ownerTheatres,
        statusCode: 200
    })
}


module.exports = {
    addTheatre,
    listTheatre,
    movieTheatres,
    ownerTheatre
}