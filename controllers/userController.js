const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
const User = require("../models/userSchema");

const createToken = (user) => {
    return jwt.sign(
      {
        id: user._id,
        role: user.role,
      },
      process.env.SECRET_KEY,
      {
        expiresIn: "2h"
      }
    );
  };

const login = async (req, res, next) => {

    res.send({
        ...req.user,
        token: createToken(req.user),
    });
};

const signUp = async (req, res, next) => {
    let {email, username, password, role}= req.body
    try {
        let userExists = await User.exists({email: email})
        console.log(userExists)
        if(userExists) {
            return next({apiStatus:"FAILURE", errorMessage:"EMAIL_ALREADY_EXISTS", statusCode:409})
        }
        let usernameExists = await User.exists({username: username})
        if(usernameExists) {
            return next({apiStatus:"FAILURE", errorMessage:"USERNAME_ALREADY_EXISTS", statusCode:409})
        }
        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(password, salt);

        let newUser = new User({
            email,
            username,
            password: hash,
            role: role
        })
        await newUser.save()
        return next({apiStatus:"SUCCESS", data: newUser, statusCode:200})
    } catch (err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

const forgetPassword = async(req, res, next) => {
    let {email, username, newPassword} = req.body
    try {
        let user = await User.findOne({email: email, username: username})
        console.log(user.password)
        if(user == null) {
            return next({apiStatus: "FAILURE", errorMessage:"USERNAME_DOESN'T_EXISTS", statusCode: 404})
        }
        let salt = bcrypt.genSaltSync(10);
        let hash = bcrypt.hashSync(newPassword, salt);
        console.log(hash)
        await user.updateOne({password: hash})
        return next({apiStatus:"SUCCESS", data:"PASSWORD_UPDATED_SUCCESSFULLY", statusCode: 200})
    }catch (err) {
        return next({
            apiStatus: "FAILURE",
            errorMessage: err.message,
            statusCode: (err.StatusCode == undefined) ? 400 : err.StatusCode,
        })
    }
}

module.exports ={
    signUp,
    login,
    forgetPassword
}