const mongoose = require("mongoose")

let Theatre = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    location: {
        type: String,
        required: true
    },
    ownerId: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("theatres", Theatre)
module.exports = mongoose.model("theatres")