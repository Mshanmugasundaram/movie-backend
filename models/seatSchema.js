const mongoose = require("mongoose")

let Seat = new mongoose.Schema({
    movieId: {
        type: String,
        required: true
    },
    theatreId: {
        type: String,
        required: true
    },
    seatNumber: {
        type: Number,
        required: true
    },
    available: {
        type: Boolean,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("seats", Seat)
module.exports = mongoose.model("seats")