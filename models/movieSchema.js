const mongoose = require("mongoose")

let Movie = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    movieImage:{
        type: String,
    }
}, {
    timestamps: true
})

mongoose.model("movies", Movie)
module.exports = mongoose.model("movies")