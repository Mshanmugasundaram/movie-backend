const mongoose = require("mongoose")

let MovieTheatre = new mongoose.Schema({
    movieId: {
        type: String,
        required: true
    },
    theatreId: {
        type: String,
        required: true
    },
    seat: {
        type: String,
        required: true
    },
    reserved: {
        type: Array,
        required: true
    }
}, {
    timestamps: true
})

mongoose.model("movietheatre", MovieTheatre)
module.exports = mongoose.model("movietheatre")