const mongoose = require("mongoose")

let UserSeat = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    movieId: {
        type: String,
        required: true
    },
    theatreId: {
        type: String,
        required: true
    },
    seatNumber: {
        type: Number,
        required: true
    },
}, {
    timestamps: true
})

mongoose.model("userSeats", UserSeat)
module.exports = mongoose.model("userSeats")